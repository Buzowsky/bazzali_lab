import java.util.Iterator;
import java.util.Vector;

public class VectorDemo {

	public static void main(String[] args) {

		int[] numeri = new int[3];
		numeri[0] = 33;
		numeri[1] = 20;
		numeri[2] = 50;
		
		System.out.println(numeri);
		
		Vector<Integer> numeri2 = new Vector<>();
		numeri2.add(33);
		numeri2.add(20);
		numeri2.add(50);
		numeri2.add(100); //il vector si ridimensione automaticamente!!
		
		System.out.println(numeri2);
		
		numeri2.remove(1); //rimuove l'elemento di indice 1 ricompattando il vector
		//numeri2.remove(33); //errore indexOutOfBounds
		
		System.out.println(numeri2);
		
		Integer n2 = new Integer(33); //i numeri interi sono oggetti
		int n = 33; //pi� comodo ma n non � un oggetto
		
		int index = numeri2.indexOf(n2); //ricerca di un oggetto nel vector
		System.out.println(index);
		
		numeri2.remove(n2);
		System.out.println(numeri2);
		
		for (Integer integer : numeri2) {
			
			System.out.println(integer);
		}
		
		for (int i = 0; i < numeri2.size(); i++) {
			
			Integer integer = numeri2.get(i);
			System.out.println(integer);
			
		}
		

	}

}
