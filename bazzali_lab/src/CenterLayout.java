
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager;

public class CenterLayout implements LayoutManager{
	
	private Component[] components;
	
	@Override
	public void addLayoutComponent(String arg0, Component arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void layoutContainer(Container parents) {
		// TODO Auto-generated method stub
		components = parents.getComponents();
		
		int width = parents.getWidth()/10;
		int height = parents.getHeight()/10;
		int x = parents.getWidth()/2 - width/2;
		int y = parents.getHeight()/2 - height/2;
		
		components[components.length-1].setBounds(x,y,width,height); 
		
	}

	@Override
	public Dimension minimumLayoutSize(Container arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Dimension preferredLayoutSize(Container arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void removeLayoutComponent(Component arg0) {
		// TODO Auto-generated method stub
		
	}
	
}
