import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager;

public class CircleLayout implements LayoutManager{

	private Component[] components;
	
	@Override
	public void addLayoutComponent(String arg0, Component arg1) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void layoutContainer(Container parent) {
		int xCentro=(parent.getWidth()/2),yCentro=(parent.getHeight()/2),r=(parent.getHeight()*2)/5;
		components = parent.getComponents();
		double k = 360/components.length;	
		for(int i=0; i<components.length; i++)
			components[i].setBounds((int)(xCentro+(r*Math.cos(((k*i)*Math.PI)/180))-(components[i].getWidth()/2)),(int)(yCentro+(r*Math.sin(((k*i)*Math.PI)/180))-(components[i].getHeight()/2)),parent.getWidth()/components.length,parent.getHeight()/components.length);
	}
	
	@Override
	public Dimension minimumLayoutSize(Container parent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Dimension preferredLayoutSize(Container parent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void removeLayoutComponent(Component comp) {
		// TODO Auto-generated method stub
		
	}
	
}
