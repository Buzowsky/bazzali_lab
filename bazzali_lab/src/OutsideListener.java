import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

public class OutsideListener implements ActionListener {

	public OutsideListener(JFrame frame) {
		super();
		this.frame = frame;
	}

	private JFrame frame;

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
		frame.getContentPane().setBackground(Color.RED);
		
	}

}
