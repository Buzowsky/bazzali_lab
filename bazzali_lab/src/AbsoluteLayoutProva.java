import javax.swing.*;

public class AbsoluteLayoutProva {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		JFrame finestra = new JFrame("AbsoluteLayoutProva");
		JButton b1 = new JButton("one");
		JButton b2 = new JButton("two");
		JButton b3 = new JButton("three");
		
		finestra.setLayout(null);
		finestra.setSize(300, 125);
		finestra.setVisible(true);
		
		b1.setBounds(25, 5, 60, 30);
		b2.setBounds(50, 40, 60, 30);
		b3.setBounds(150, 15, 120, 50);
		
		finestra.add(b1);
		finestra.add(b2);
		finestra.add(b3);
	}

}
