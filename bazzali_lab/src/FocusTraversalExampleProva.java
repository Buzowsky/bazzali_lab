import javax.swing.*;
import java.awt.*;

public class FocusTraversalExampleProva {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		GridLayout gridLayout = new GridLayout(6,1);
		JFrame finestra = new JFrame("FocusTraversalProva");
		JButton b1 = new JButton("Texas");
		JButton b2 = new JButton("Vermont");
		JButton b3 = new JButton("Florida");
		JButton b4 = new JButton("IOWA");
		JButton b5 = new JButton("Minnesota");
		JButton b6 = new JButton("California");
		
		finestra.setLayout(gridLayout);
		finestra.setSize(400, 300);
		finestra.setVisible(true);
		
		finestra.add(b1);
		finestra.add(b2);
		finestra.add(b3);
		finestra.add(b4);
		finestra.add(b5);
		finestra.add(b6);
		
	}

}
