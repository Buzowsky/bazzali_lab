package lab15_12_2016;

public class Classe {
	
	public String sez, ind;
	public int anno;
	
	public String getSez() {
		return sez;
	}
	
	public void setSez(String sez) {
		this.sez = sez;
	}
	
	public String getInd() {
		return ind;
	}
	
	public void setInd(String ind) {
		this.ind = ind;
	}
	
	public int getAnno() {
		return anno;
	}
	
	public void setAnno(int anno) {
		this.anno = anno;
	}
	
}
