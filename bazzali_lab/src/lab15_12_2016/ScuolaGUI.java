package lab15_12_2016;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class ScuolaGUI {

	private JFrame frame;
	
	public ScuolaGUI(){
		super();
		frame = new JFrame("GUISchool1");
		
		//JPanel p = new JPanel();
		//JPanel p1 = new JPanel();
		//JPanel annoP = new JPanel();
		Box box1 = Box.createVerticalBox(); 
		Box box2 = Box.createVerticalBox(); 
		Box box3 = Box.createVerticalBox(); 
		box2.add(box3, BorderLayout.NORTH);
		JPanel sezP = new JPanel();
		box2.add(sezP, BorderLayout.NORTH);
		JPanel indP = new JPanel();
		box2.add(indP, BorderLayout.NORTH);
		
		GridLayout grid = new GridLayout(3,2);
		frame.add(box2);
		box2.setLayout(grid);
		
		JButton add= new JButton("Aggiungi");
		JButton salva= new JButton("Salva");
		JButton canc = new JButton("Cancella");
		
		JLabel anno = new JLabel("Anno");
		box3.add(anno);
		JLabel sez = new JLabel("Sezione");
		sezP.add(sez);
		JLabel ind = new JLabel("Indirizzo");
		indP.add(ind);
		
		JTextField a = new JTextField(10);
		box3.add(a);
		
		JTextField b = new JTextField(10);
		sezP.add(b);
		JTextField c = new JTextField(10);
		indP.add(c);
		
		frame.add(box2, BorderLayout.NORTH);
		frame.add(box1, BorderLayout.SOUTH);
		
		box1.add(add);
		box1.add(canc);
		box1.add(salva);
		
		salva.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				System.out.println("Anno: " + a.getText() + ", sezione: " + b.getText() + ", indirizzo: " + c.getText());
			}
		});

		canc.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				a.setText("");
				b.setText("");
				c.setText("");
			}
		});
		
		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
