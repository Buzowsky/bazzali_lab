package lab15_12_2016;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class GUIschool1 {
	
	private JFrame frame;
	
	public GUIschool1(){
		super();
		frame = new JFrame("GUISchool1");
		
		JPanel p = new JPanel();
		JPanel p1 = new JPanel();
		JPanel annoP = new JPanel();
		p1.add(annoP, BorderLayout.NORTH);
		JPanel sezP = new JPanel();
		p1.add(sezP, BorderLayout.NORTH);
		JPanel indP = new JPanel();
		p1.add(indP, BorderLayout.NORTH);
		
		GridLayout grid = new GridLayout(3,2);
		frame.add(p1);
		p1.setLayout(grid);
		
		JButton add= new JButton("Aggiungi");
		JButton salva= new JButton("Salva");
		JButton canc = new JButton("Cancella");
		
		JLabel anno = new JLabel("Anno");
		annoP.add(anno);
		JLabel sez = new JLabel("Sezione");
		sezP.add(sez);
		JLabel ind = new JLabel("Indirizzo");
		indP.add(ind);
		
		JTextField a = new JTextField(10);
		annoP.add(a);
		
		JTextField b = new JTextField(10);
		sezP.add(b);
		JTextField c = new JTextField(10);
		indP.add(c);
		
		frame.add(p1, BorderLayout.NORTH);
		frame.add(p, BorderLayout.SOUTH);
		
		p.add(add);
		p.add(canc);
		p.add(salva);
		
		salva.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				System.out.println("Anno: " + a.getText() + ", sezione: " + b.getText() + ", indirizzo: " + c.getText());
			}
		});

		canc.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				a.setText("");
				b.setText("");
				c.setText("");
			}
		});
		
		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				new GUIschool1();
				
			}
		});
	}
}
