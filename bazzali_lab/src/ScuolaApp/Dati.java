package ScuolaApp;

import java.io.Serializable;
import java.util.Vector;

public class Dati implements Serializable {

	private Vector<Alunno> alunni;
	private Vector<Classe> classi;
	
	//aggiungere alunni
	public boolean add(Alunno e) {
		return alunni.add(e);
	}

	//aggiungere classi
	public boolean add(Classe e) {
		return classi.add(e);
	}
	
}
