package ScuolaApp;

import java.io.Serializable;

/*
 * Serializable serve per salvare su file
 * le istanze della classe
 */
public class Alunno implements Serializable {
	
	private String nome, cognome;
	private Classe classe;

	public Alunno(String nome, String cognome) {
		super();
		this.nome = nome;
		this.cognome = cognome;
	}

	public void setClasse(Classe classe) {
		this.classe = classe;
	}

	@Override
	public String toString() {
		return nome + " " + cognome + " " + classe;
	}
	
	public static void main(String[] args) {
		
		
		Alunno alunno = new Alunno("Gianni", "Rossi");
		Classe classe = new Classe(1, 'B', "INFORMATICA");
		alunno.setClasse(classe);
		
		System.out.println(alunno);
		
		
	}

}
