package ScuolaApp;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class DataInput {
	
	private JFrame frame;
	private JTextField annoField, sezioneField, indirizzoField;
	private Classe classe;

	class SaveListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			
			int anno = Integer.parseInt(annoField.getText());
			char sezione = sezioneField.getText().charAt(0);
			String indirizzo = indirizzoField.getText();
			
			classe = new Classe(anno, sezione, indirizzo);
			System.out.println(classe);
			
			annoField.setText("");
			sezioneField.setText("");
			indirizzoField.setText("");
			
		}
		
	}
	
	class CancelListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			
			annoField.setText("");
			sezioneField.setText("");
			indirizzoField.setText("");
						
		}
		
	}

	public DataInput() {
		super();
		frame = new JFrame("progetto GUI scuola1");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JButton button = new JButton("SALVA");
		JButton button2 = new JButton("CANCELLA");
		JPanel southPanel = new JPanel();
		southPanel.add(button);
		southPanel.add(button2);
		frame.getContentPane().add(southPanel, BorderLayout.SOUTH);
		button.addActionListener(new SaveListener());
		button2.addActionListener(new CancelListener());
		
		JPanel centerPanel = new JPanel();
		JPanel gridPanel = new JPanel(new GridLayout(3, 2));
		centerPanel.add(gridPanel);
		
		JPanel annoPanel, sezionePanel, indirizzoPanel;
		annoPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 10));
		sezionePanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 10));
		indirizzoPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 10));
		
		JLabel annoLabel = new JLabel("Anno");
		annoField = new JTextField(1);
		annoPanel.add(annoLabel);
		annoPanel.add(annoField);
		gridPanel.add(annoPanel);
		
		JLabel sezioneLabel = new JLabel("Sezione");
		sezioneField = new JTextField(1);
		sezionePanel.add(sezioneLabel);
		sezionePanel.add(sezioneField);
		gridPanel.add(sezionePanel);
		
		JLabel indirizzoLabel = new JLabel("Indirizzo");
		indirizzoField = new JTextField(10);
		indirizzoPanel.add(indirizzoLabel);
		indirizzoPanel.add(indirizzoField);
		gridPanel.add(indirizzoPanel);
		
		frame.getContentPane().add(centerPanel);
		frame.setVisible(true);
	}
	
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				new DataInput();
				
			}
		});
	}
}


