import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager;

public class DiagonalLayout implements LayoutManager{

	private Component[] components;
	
	@Override
	public void addLayoutComponent(String arg0, Component arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void layoutContainer(Container parents) {
		// TODO Auto-generated method stub
		components = parents.getComponents();
		
		int x,y;
		int width = parents.getWidth()/components.length;
		int height = parents.getHeight()/components.length;
		
		for(int i=0; i<components.length; i++){
			x = width*i;
			y = height*i;
			components[i].setBounds(x,y,width,height);
		} 
		
	}	

	@Override
	public Dimension minimumLayoutSize(Container parent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Dimension preferredLayoutSize(Container parent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void removeLayoutComponent(Component comp) {
		// TODO Auto-generated method stub
		
	}

}
