import javax.swing.*;
import java.awt.*;

public class FlowLayoutProva {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		FlowLayout flowLayout = new FlowLayout();
		JFrame finestra = new JFrame("FlowLayoutProva");
		JButton b1 = new JButton("1");
		JButton b2 = new JButton("2");
		JButton b3 = new JButton("3");
		JButton b4 = new JButton("4");
		JButton b5 = new JButton("5");
		JButton b6 = new JButton("6");
		JButton b7 = new JButton("7");
		JButton b8 = new JButton("8");
		JButton b9 = new JButton("9");
		
		finestra.setLayout(flowLayout);
		finestra.setSize(326, 256);
		finestra.setVisible(true);
		
		finestra.add(b1);
		finestra.add(b2);
		finestra.add(b3);
		finestra.add(b4);
		finestra.add(b5);
		finestra.add(b6);
		finestra.add(b7);
		finestra.add(b8);
		finestra.add(b9);
		
	}

}
