import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class GraphicPanel extends JPanel {

	@Override
	protected void paintComponent(Graphics g) {
		// TODO Auto-generated method stub
		super.paintComponent(g);
		
		int x;
		int width = this.getWidth();
		x = width/2;
		int y;
		int height = this.getHeight();
		y = height/2;
		
		/*
		//g.fillRect(x, y, 50 , 50 );
		
		//String str = "Ciao";
		
		Font f = new Font("TimesRoman", Font.PLAIN, 30);
		g.setFont(f);
		
		//Font font = new Font("SanSerif", Font.BOLD, 100);
		//g.setFont(font );

		//g.drawString(str , 30, 100);
		
		g.setColor(Color.RED);
		g.fillOval(50, 50, 500, 500);
		
		g.setColor(Color.BLACK);
		g.fillOval(100, 100, 400, 400);
		*/
		
		Image image = null;
		try {
		image  = ImageIO.read(new File("images/01.jpg"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		g.drawImage(image, x, y, null);

		
	}

}
