import javax.swing.*;
import java.awt.*;

public class BorderLayoutProva {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		BorderLayout borderLayout = new BorderLayout();
		JFrame finestra = new JFrame("BorderLayoutProva");
		JButton b1 = new JButton("North");
		JButton b2 = new JButton("South");
		JButton b3 = new JButton("East");
		JButton b4 = new JButton("West");
		JButton b5 = new JButton("Center");
		
		finestra.setLayout(borderLayout);
		finestra.setSize(241, 161);
		finestra.setVisible(true);
		
		finestra.add(b1, BorderLayout.NORTH);
		finestra.add(b2, BorderLayout.SOUTH);
		finestra.add(b3, BorderLayout.EAST);
		finestra.add(b4, BorderLayout.WEST);
		finestra.add(b5, BorderLayout.CENTER);
		
		
	}

}
