import javax.swing.*;
import java.awt.*;

public class GridLayoutProva {

	public static void main(String[] args) {
		
		GridLayout gridLayout = new GridLayout(3,4);
		JFrame finestra = new JFrame("GridLayoutProva");
		JButton b1 = new JButton("B1");
		JButton b2 = new JButton("B2");
		JButton b3 = new JButton("B3");
		JButton b4 = new JButton("B4");
		JButton b5 = new JButton("B5");
		JButton b6 = new JButton("B6");
		JButton b7 = new JButton("B7");
		JButton b8 = new JButton("B8");
		JButton b9 = new JButton("B9");
		JButton b10 = new JButton("B10");
		JButton b11 = new JButton("B11");
		JButton b12 = new JButton("B12");
		
		finestra.setLayout(gridLayout);
		
		finestra.add(b1);
		finestra.add(b2);
		finestra.add(b3);
		finestra.add(b4);
		finestra.add(b5);
		finestra.add(b6);
		finestra.add(b7);
		finestra.add(b8);
		finestra.add(b9);
		finestra.add(b10);
		finestra.add(b11);
		finestra.add(b12);
		
		finestra.setVisible(true);
		finestra.setSize(600, 400);
		
	}

}
