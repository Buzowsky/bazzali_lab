import javax.swing.*;
import java.awt.*;

public class GridCalcProva {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		GridLayout gridLayout = new GridLayout(5,4,5,5);
			
		JFrame finestra = new JFrame("GridCalcProva");
		
		JButton b0 = new JButton("0");
		JButton b1 = new JButton("1");
		JButton b2 = new JButton("2");
		JButton b3 = new JButton("3");
		JButton b4 = new JButton("4");
		JButton b5 = new JButton("5");
		JButton b6 = new JButton("6");
		JButton b7 = new JButton("7");
		JButton b8 = new JButton("8");
		JButton b9 = new JButton("9");
		JButton cls = new JButton("Cls");
		JButton pt = new JButton(".");
		JButton bck = new JButton("Bck");
		JButton close = new JButton("Close");
		JButton equ = new JButton("=");
		JButton plus = new JButton("+");
		JButton minus = new JButton("-");
		JButton mul = new JButton("*");
		JButton div = new JButton("/");
		
		JLabel label = new JLabel("");
		
		finestra.setLayout(gridLayout);
		finestra.setSize(350,300);
		finestra.setVisible(true);
		
		finestra.add(cls);
		finestra.add(bck);
		finestra.add(label);
		finestra.add(close);
		finestra.add(b7);
		finestra.add(b8);
		finestra.add(b9);
		finestra.add(div);
		finestra.add(b4);
		finestra.add(b5);
		finestra.add(b6);
		finestra.add(mul);
		finestra.add(b1);
		finestra.add(b2);
		finestra.add(b3);
		finestra.add(minus);
		finestra.add(b0);
		finestra.add(pt);
		finestra.add(equ);
		finestra.add(plus);
		
	}

}
